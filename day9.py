from os import times
import queue

def read_data():
    with open("day9.csv") as file:
        data = []
        for line in file:
            data.append(list(map(int, list(line.rstrip("\n")))))
        print(data)
        return data

def main():
    data = read_data()
    risk = []
    for i in range(len(data)):
        for j in range(len(data[i])):
            if i == 0 and j == 0:
                if data[i][j] < data[i][j + 1] and data[i][j] < data[i + 1][j]:
                    risk.append(data[i][j] + 1)
            elif i == 0 and j == len(data[i])-1:
                if data[i][j] < data[i][j - 1] and data[i][j] < data[i + 1][j]:
                    risk.append(data[i][j] + 1)
            elif i == 0 and j > 0:
                if data[i][j] < data[i][j - 1] and data[i][j] < data[i][j + 1] and data[i][j] < data[i + 1][j]:
                    risk.append(data[i][j] + 1)
            
            elif i == len(data) - 1 and j == 0:
                if data[i][j] < data[i][j + 1] and data[i][j] < data[i - 1][j]:
                    risk.append(data[i][j] + 1)
            elif i == len(data) - 1 and j == len(data[i]) - 1:
                if data[i][j] < data[i][j - 1] and data[i][j] < data[i - 1][j]:
                    risk.append(data[i][j] + 1)
            elif i == len(data) - 1 and j > 0:
                if data[i][j] < data[i][j - 1] and data[i][j] < data[i][j + 1] and data[i][j] < data[i - 1][j]:
                    risk.append(data[i][j] + 1)
            
            elif i > 0 and j == 0:
                if data[i][j] < data[i][j + 1] and data[i][j] < data[i - 1][j] and data[i][j] < data[i + 1][j]:
                    risk.append(data[i][j] + 1)
            elif i > 0 and j == len(data[i]) - 1:
                if data[i][j] < data[i][j - 1] and data[i][j] < data[i - 1][j] and data[i][j] < data[i + 1][j]:
                    risk.append(data[i][j] + 1)
            elif i > 0 and j > 0:
                if data[i][j] < data[i][j + 1] and data[i][j] < data[i][j - 1]and data[i][j] < data[i - 1][j] and data[i][j] < data[i + 1][j]:
                    risk.append(data[i][j] + 1)
    print(risk)
    print(sum(risk))

def main2():
    data = read_data()
    bassins = [[[] for _ in range(len(data[0]))] for _ in range(len(data))]
    print(bassins)
    q = queue.SimpleQueue()
    for i in range(len(data)):
        for j in range(len(data[i])):
            q.put((i, j))
            # print("new")
            while not q.empty():
                (x, y) = q.get()
                # print(x, y)
                if (x, y) not in bassins[i][j]: 
                    bassins[i][j].append((x, y))
                
                if data[x][y] == 9:
                    continue
                elif x == 0 and y == 0:
                    if abs(data[x][y] - data[x + 1][y]) == 1 and data[x + 1][y] != 9:
                        if (x + 1, y) not in bassins[i][j]: q.put((x + 1, y))
                    if abs(data[x][y] - data[x][y + 1]) == 1 and data[x][y + 1] != 9:
                        if (x, y + 1) not in bassins[i][j]: q.put((x, y + 1))
                elif x == 0 and y == len(data[0]) - 1:
                    if abs(data[x][y] - data[x + 1][y]) == 1  and data[x + 1][y] != 9:
                        if (x + 1, y) not in bassins[i][j]: q.put((x + 1, y))
                    if abs(data[x][y] - data[x][y - 1]) == 1 and data[x][y - 1] != 9:
                        if (x, y - 1) not in bassins[i][j]: q.put((x, y - 1))
                elif x == 0 and y > 0 and y < len(data[0]) - 1:
                    if abs(data[x][y] - data[x + 1][y]) == 1 and data[x + 1][y] != 9:
                        if (x + 1, y) not in bassins[i][j]: q.put((x + 1, y))
                    if abs(data[x][y] - data[x][y - 1]) and data[x][y - 1] != 9:
                        if (x, y - 1) not in bassins[i][j]: q.put((x, y - 1))
                    if abs(data[x][y] - data[x][y + 1]) == 1 and data[x][y + 1] != 9:
                        if (x, y + 1) not in bassins[i][j]: q.put((x, y + 1))
                
                elif x == len(data) - 1 and y == 0:
                    if abs(data[x][y] - data[x][y + 1]) == 1 and data[x][y + 1] != 9:
                        if (x, y + 1) not in bassins[i][j]: q.put((x, y + 1))
                    if abs(data[x][y] - data[x - 1][y]) == 1 and data[x - 1][y] != 9:
                        if (x - 1, y) not in bassins[i][j]: q.put((x - 1, y))
                elif x == len(data) - 1 and y == len(data[0]) - 1:
                    if abs(data[x][y] - data[x][y - 1]) == 1 and data[x][y - 1] != 9:
                        if (x, y - 1) not in bassins[i][j]: q.put((x, y - 1))
                    if abs(data[x][y] - data[x - 1][y]) == 1 and data[x - 1][y] != 9:
                        if (x - 1, y) not in bassins[i][j]: q.put((x - 1, y))
                elif x == len(data) - 1 and y > 0 and y < len(data[0]) - 1:
                    if abs(data[x][y] - data[x][y - 1]) == 1  and data[x][y - 1] != 9:
                        if (x, y - 1) not in bassins[i][j]: q.put((x, y - 1))
                    if abs(data[x][y] - data[x - 1][y]) == 1 and data[x - 1][y] != 9:
                        if (x - 1, y) not in bassins[i][j]: q.put((x - 1, y))
                    if abs(data[x][y] - data[x][y + 1]) == 1 and data[x][y + 1] != 9:
                        if (x, y + 1) not in bassins[i][j]: q.put((x, y + 1))
                
                elif x > 0 and x < len(data) - 1 and y == 0:
                    if abs(data[x][y] - data[x - 1][y]) == 1  and data[x - 1][y] != 9:
                        if (x - 1, y) not in bassins[i][j]: q.put((x - 1, y))
                    if abs(data[x][y] - data[x + 1][y]) == 1 and data[x + 1][y] != 9:
                        if (x + 1, y) not in bassins[i][j]: q.put((x + 1, y))
                    if abs(data[x][y] - data[x][y + 1]) == 1 and data[x][y + 1] != 9:
                        if (x, y + 1) not in bassins[i][j]: q.put((x, y + 1))
                elif x > 0 and x < len(data) - 1 and y == len(data[0]) - 1:
                    if abs(data[x][y] - data[x - 1][y]) == 1 and data[x - 1][y] != 9:
                        if (x - 1, y) not in bassins[i][j]: q.put((x - 1, y))
                    if abs(data[x][y] - data[x + 1][y]) == 1 and data[x + 1][y] != 9:
                        if (x + 1, y) not in bassins[i][j]: q.put((x + 1, y))
                    if abs(data[x][y] - data[x][y - 1]) == 1 and data[x][y - 1] != 9:
                        if (x, y - 1) not in bassins[i][j]: q.put((x, y - 1))
                elif x > 0 and x < len(data) - 1 and y > 0 and y < len(data[0]) - 1:
                    if abs(data[x][y] - data[x][y + 1]) == 1 and data[x][y + 1] != 9:
                        if (x, y + 1) not in bassins[i][j]: q.put((x, y + 1))
                    if abs(data[x][y] - data[x - 1][y]) == 1  and data[x - 1][y] != 9:
                        if (x - 1, y) not in bassins[i][j]: q.put((x - 1, y))
                    if abs(data[x][y] - data[x + 1][y]) == 1 and data[x + 1][y] != 9:
                        if (x + 1, y) not in bassins[i][j]: q.put((x + 1, y))
                    if abs(data[x][y] - data[x][y - 1]) == 1 and data[x][y - 1] != 9:
                        if (x, y - 1) not in bassins[i][j]: q.put((x, y - 1))
    # print(bassins)
    sorter = []
    seen = set()
    for b in bassins:
        for c in b:
            if tuple(sorted(c)) not in seen and len(c) > 1:
                sorter.append(sorted(c))
                seen.add(tuple(sorted(c)))
    highest = (0, 0, 0)
    saved = 0
    for b in sorter:
        if len(b) > 60:
            print(len(b))
            saved += 1
        if len(b) > highest[0]:
            highest = (len(b), highest[0], highest[1])
        elif len(b) > highest[1]:
            highest = (highest[0], len(b), highest[1])
        elif len(b) > highest[2]:
            highest = (highest[0], highest[1], len(b))
    print(saved)
    print(highest)
    print(highest[0] * highest[1] * highest[2]) # 287028, 291584





if __name__ == "__main__":
    main2()
