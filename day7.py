import csv

def read_data():
    with open("day7.csv") as file:
        reader = csv.reader(file)
        data = []
        max = 0
        row = next(reader)
        for column in row:
            if max < int(column):
                max = int(column)
            data.append(int(column))
        print(data)
        print(max)
        return max, data

def main2():
    max, data = read_data()
    totals = []
    for i in range(max):
        print(str(i) + " out of " + str(max))
        total = 0
        distance = 0
        for d in data:
            distance = abs(d - i) + 1
            for j in range(distance):
                total += j
        totals.append((i, total))
    print(totals)

    lowest = (float("inf"), float("inf"))
    for (n, t) in totals:
        if t < lowest[1]:
            lowest = (n, t)
    
    print(lowest)

def main():
    max, data = read_data()
    totals = []
    for i in range(max):
        total = 0
        for d in data:
            total += abs(d - i)
        totals.append((i, total))
    print(totals)

    lowest = (float("inf"), float("inf"))
    for (n, t) in totals:
        if t < lowest[1]:
            lowest = (n, t)
    
    print(lowest)

if __name__ == "__main__":
    main2()

