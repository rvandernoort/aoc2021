import csv

days = 256

def read_data():
  with open("day6.csv") as file:
    reader = csv.reader(file)
    data = dict([(0, 0), (1, 0), (2, 0), (3, 0), (4, 0), (5, 0), (6, 0), (7, 0), (8, 0)])
    print(data)
    for row in reader:
      for column in row:
        data.append(int(column))
    print(data)
    return data

def read_data2():
  with open("day6.csv") as file:
    reader = csv.reader(file)
    data = dict([(0, 0), (1, 0), (2, 0), (3, 0), (4, 0), (5, 0), (6, 0), (7, 0), (8, 0)])
    print(data)
    for row in reader:
      for column in row:
        data[int(column)] += 1
        # data.append(int(column))
    print(data)
    return data

def minus(n, added_data):
  if n <= 0:
    added_data.append(8)
    return 6
  return n-1

def next_day(data):
  updated_data = []
  added_data = []
  for d in data:
    if d <= 0:
      updated_data.append(6)
      added_data.append(8)
    else:
      updated_data.append(d-1)
  updated_data.extend(added_data)
  print(updated_data)
  return updated_data

def main():
  added_data = []
  data = read_data()
  for d in range(days):
    data = list(map(lambda p: minus(p, added_data), data))
    data.extend(added_data)
    added_data = []
  print(len(data))

def main2():
  data = read_data2()
  for d in range(days):
    new_data = dict([(0, 0), (1, 0), (2, 0), (3, 0), (4, 0), (5, 0), (6, 0), (7, 0), (8, 0)])
    print(str(d) + " out of " + str(days))
    for k in data:
      if k == 0:
        new_data[6] += data[k]
        new_data[8] += data[k]
      else:
        new_data[k-1] += data[k]
    data = new_data.copy()

  total = 0
  for d in data:
    total += data[d]
  print(total)

if __name__ == "__main__":
  main2()
