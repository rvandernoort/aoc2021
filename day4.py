import csv

def read_data():
    with open("day4.csv") as file:
        reader = csv.reader(file)
        drawn = []
        column = next(reader)
        drawn.extend([c for c in column])
        print(drawn)
        readerb = csv.reader(file, delimiter=' ', skipinitialspace=True)
        next(readerb)
        # next(readerb)
        boards = []
        n = 5
        m = 5
        board = []
        for row in readerb:
            if n <= 0:
                # next(readerb)
                n = 6
                boards.append(board)
                board = []
            else:
                # print(row)
                board.append([(row[i], 0) for i in range(m)])
            n -= 1
        boards.append(board)
        # print(boards)
        return (drawn, boards)

def main2():
    (drawn, boards) = read_data()
    won_boards = []
    last_board = -1
    last_draw = -1
    for d in drawn:
        if last_board != -1:
            break
        newboards = boards.copy()
        for b in range(len(boards)):
            for r in range(len(boards[b])):
                for c in range(len(boards[b][r])):
                    (n, v) = boards[b][r][c]
                    if n == d:
                        newboards[b][r][c] = (n, 1)
        # print("draw " + str(d) + " " + str(newboards[2]))

        for b in range(len(newboards)):
            for r in range(len(newboards[b])):
                cc = 0
                for c in range(len(newboards[b][r])):
                    (n, v) = boards[b][r][c]
                    if v == 1:
                        cc += 1
                if cc >= 5:
                    print("rowbingo in row " + str(r) + " in board " + str(b) + " at draw " + str(d))
                    if last_board == -1 and b not in won_boards:
                        won_boards.append(b)
                        if len(won_boards) == len(boards):
                            last_board = b
                            last_draw = d

            for c in range(len(newboards[b][0])):
                rc = 0
                for r in range(len(newboards[b])):
                    (n, v) = boards[b][r][c]
                    if v == 1:
                        rc += 1
                if rc >= 5:
                    print("columnbingo in colum " + str(c) + " in board " + str(b) + " at draw " + str(d))
                    if last_board == -1 and b not in won_boards:
                        won_boards.append(b)
                        if len(won_boards) == len(boards):
                            last_board = b
                            last_draw = d
    print(last_board)
    print(last_draw)

    sum_board = 0
    for r in range(len(newboards[last_board])):
        for c in range(len(newboards[last_board][r])):
            (n, v) = boards[last_board][r][c]
            if v == 0:
                sum_board += int(n)
    

    print(sum_board * int(last_draw))

def main():
    (drawn, boards) = read_data()
    winning_board = -1
    winning_draw = -1
    for d in drawn:
        if winning_board != -1:
            break
        newboards = boards.copy()
        for b in range(len(boards)):
            for r in range(len(boards[b])):
                for c in range(len(boards[b][r])):
                    (n, v) = boards[b][r][c]
                    if n == d:
                        newboards[b][r][c] = (n, 1)
        print("draw " + str(d) + " " + str(newboards[2]))

        for b in range(len(newboards)):
            for r in range(len(newboards[b])):
                cc = 0
                for c in range(len(newboards[b][r])):
                    (n, v) = boards[b][r][c]
                    if v == 1:
                        cc += 1
                if cc >= 5:
                    print("rowbingo in row " + str(r) + " in board " + str(b) + " at draw " + str(d))
                    if winning_board == -1:
                        winning_board = b
                        winning_draw = d

            for c in range(len(newboards[b][0])):
                rc = 0
                for r in range(len(newboards[b])):
                    (n, v) = boards[b][r][c]
                    if v == 1:
                        rc += 1
                if rc >= 5:
                    print("columnbingo in colum " + str(c) + " in board " + str(b) + " at draw " + str(d))
                    if winning_board == -1:
                        winning_board = b
                        winning_draw = d
    print(winning_board)
    print(winning_draw)

    sum_board = 0
    for r in range(len(newboards[winning_board])):
        for c in range(len(newboards[winning_board][r])):
            (n, v) = boards[winning_board][r][c]
            if v == 0:
                sum_board += int(n)
    
    print(sum_board * int(winning_draw))

if __name__ == '__main__':
    main2()
