import csv

filename = "day1.csv"

def read_data():
  with open(filename) as file: 
    reader = csv.reader(file)
    data = []
    for line in reader:
      data.append(int(line[0]))
    return data

def read_data2():
  with open(filename) as file: 
    reader = csv.reader(file)
    data = []
    first = None
    second = None
    third = None
    for line in reader:
      if first is None:
        first = int(line[0])
      elif second is None:
        second = int(line[0])
      elif third is None:
        third = int(line[0])
        data.append(first + second + third)
      else:
        first = second
        second = third
        third = int(line[0])
        data.append(first + second + third)
    return data

def main2():
  data = read_data2()
  print(data)
  print(len(data))
  count = sum(data[i] > data[i-1] for i in range(1, len(data)))
  print(count)

def main():
  data = read_data()
  print(len(data))
  count = sum(data[i] > data[i-1] for i in range(1, len(data))) 
  # count = sum(data[i+1] > data[i] for i in range(0, len(data)-1)) 
  
  # count = 0
  # for i in range(1, len(data)):
  #   if data[i] > data[i-1]:
  #     count += 1
  print(count) # ofbyone?

if __name__ == "__main__":
  main()