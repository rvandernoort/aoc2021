import re

def read_data():
    with open("day5.csv") as file:
        data = []
        max_length_x = max_length_y = 0
        for line in file:
            ll = re.split(",| |\n", line)
            data.append(((int(ll[0]), int(ll[1])), (int(ll[3]), int(ll[4]))))
            if int(ll[0]) > max_length_x: max_length_x = int(ll[0])
            if int(ll[3]) > max_length_x: max_length_x = int(ll[3])
            if int(ll[1]) > max_length_y: max_length_y = int(ll[1])
            if int(ll[4]) > max_length_y: max_length_y = int(ll[4])
        print(data)
        return ((max_length_x, max_length_y), data)

def create_grid(x, y):
    print(x)
    grid = [[0 for _ in range(x + 1)] for _ in range(y + 1)]
    # print(grid)
    return grid

def filter_data(data):
    filtered_data = []
    for ((x1, y1), (x2, y2)) in data:
        if x1 == x2 or y1 == y2:
            filtered_data.append(((x1, y1), (x2, y2)))
    print(filtered_data)
    return filtered_data

def filter_data2(data):
    filtered_data = []
    for ((x1, y1), (x2, y2)) in data:
        if x1 == x2 or y1 == y2 or abs(x1 - x2) == abs(y1 - y2):
            filtered_data.append(((x1, y1), (x2, y2)))
    print(filtered_data)
    return filtered_data

def fill_grid(data, grid):
    for ((x1, y1), (x2, y2)) in data:
        for i in range(len(grid)):
            if y1 == y2 and y1 == i:
                for j in range(len(grid[i])):
                    if (x1 < x2 and x1 <= j and x2 >= j) or (x2 < x1 and x2 <= j and x1 >= j):
                        grid[i][j] += 1
            elif x1 == x2 and ((y1 < y2 and y1 <= i and y2 >= i) or (y2 < y1 and y2 <= i and y1 >= i)):
                grid[i][x1] += 1

    print(grid)
    return grid

def fill_grid2(data, grid):
    for ((x1, y1), (x2, y2)) in data:
        for i in range(len(grid)):
            if y1 == y2 and y1 == i:
                for j in range(len(grid[i])):
                    if (x1 < x2 and x1 <= j and x2 >= j) or (x2 < x1 and x2 <= j and x1 >= j):
                        grid[i][j] += 1
            elif x1 == x2 and ((y1 < y2 and y1 <= i and y2 >= i) or (y2 < y1 and y2 <= i and y1 >= i)):
                grid[i][x1] += 1
        if abs(x1-x2) == abs(y1- y2):
            for i in range(abs(x1-x2) + 1):
                print([len(grid), len(grid[0]), x1, y1, x2, y2, i])
                if x1 < x2 and y1 < y2:
                    grid[y1 + i][x1 + i] += 1
                if x1 < x2 and y1 > y2:
                    grid[y1 - i][x1 + i] += 1
                if x1 > x2 and y1 < y2:
                    grid[y1 + i][x1 - i] += 1
                if x1 > x2 and y1 > y2:
                    grid[y1 - i][x1 - i] += 1
    for row in grid:
        print(row)
    return grid

def count_grid(grid):
    count = 0
    for row in grid:
        for column in row:
            if column >= 2:
                count += 1
    print(count)
    return count


def main2():
    ((x, y), data) = read_data()
    filtered_data = filter_data2(data)
    grid = create_grid(x, y)
    filled_grid = fill_grid2(filtered_data, grid)
    count = count_grid(filled_grid)

def main():
    ((x, y), data) = read_data()
    filtered_data = filter_data(data)
    grid = create_grid(x, y)
    filled_grid = fill_grid(filtered_data, grid)
    count = count_grid(filled_grid)

if __name__ == "__main__":
    main2()