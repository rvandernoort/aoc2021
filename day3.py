import csv

filename = "day3.csv"

def read_data():
  with open(filename) as file: 
    reader = csv.reader(file)
    data = []
    for line in reader:
      data.append(int(line[0], 2))
    return data

def main2():
  data = read_data()
  print(len(data))
  size = 12
  upper = data.copy()
  lower = data.copy()
  temp = upper.copy()
  temp2 = lower.copy()
  for i in range(size):
    if len(temp) > 1:
      (j, k) = (0, 0)
      for d in temp:
        res = d & 1 << size - 1 - i
        if res > 0:
          j += 1
        else:
          k += 1

      print(j, k)

      if j >= k:
        for d in temp:
          print("1 - " + bin(d))
          res = d & 1 << size - 1 - i
          if res <= 0:
            upper.remove(d)
            print("remove")
      else:
        for d in temp:
          print("0 - " + bin(d))
          res = d & 1 << size - 1 - i
          if res > 0:
            upper.remove(d)
            print("remove")

      temp = upper.copy()

    for u in upper:
      print("Upper: " + bin(u))

    if len(temp2) > 1:
      (l, m) = (0, 0)
      for d in temp2:
        res = d & 1 << size - 1 - i
        if res > 0:
          l += 1
        else:
          m += 1

      print((l, m))

      if l >= m:
        for d in temp2:
          print("0 - " + bin(d))
          res = d & 1 << size - 1 - i
          if res > 0:
            lower.remove(d)
            print("remove")
      else:
        for d in temp2:
          print("1 - " + bin(d))
          res = d & 1 << size - 1 - i
          if res <= 0:
            lower.remove(d)
            print("remove")

      temp2 = lower.copy()
    
    for u in lower:
      print("Lower: " + bin(u))

    support = upper[0] * lower[0]
    print(support)
          
  

def main():
  data = read_data()
  print(len(data))
  size = 12
  print(size)
  mask = int("000000000001", 2)
  count = [(0,0),(0,0),(0,0),(0,0),(0,0),(0,0),(0,0),(0,0),(0,0),(0,0),(0,0),(0,0)]
  for i in range(size):
    (j, k) = count[i]
    for d in data:
      res = d & mask
      if res > 0:
        j += 1
      else:
        k += 1
    count[i] = (j, k)
    mask = mask << 1

  upper = 0
  lower = 0
  for i, (j, k) in enumerate(count):
    if j > k:
      upper = upper | (1 << i)
    else:
      lower = lower | (1 << i)
  print(bin(upper))
  print(bin(lower))

  power = upper * lower
  print(power)

if __name__ == "__main__":
  main2()