import csv
from os import sep

filename = "day2.csv"

def read_data():
  with open(filename) as file: 
    reader = csv.reader(file, delimiter=' ')
    data = []
    for line in reader:
      data.append((line[0], int(line[1])))
    return data

def main2():
  data = read_data()
  print(data)
  hor = 0
  aim = 0
  depth = 0
  for (d, n) in data:
    if d == 'forward':
      hor += n
      depth += aim * n
    if d == 'down':
      aim += n
    if d == 'up':
      aim -= n
  print(hor * depth)

def main():
  data = read_data()
  print(data)
  hor = 0
  depth = 0
  for (d, n) in data:
    if d == 'forward':
      hor += n
    if d == 'down':
      depth += n
    if d == 'up':
      depth -= n
  print(hor * depth)
  
if __name__ == "__main__":
  main2()